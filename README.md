# Temperature Converter

This is a simple java servlet that can convert temperatures
in Celsius to Fahrenheit, and Fahrenheit to Celsius.

Made for M4 Data & Information, Web Programming Lab 1.