package TemperatureConvert;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

/**
 * Servlet that converts Celsius to Fahrenheit.
 */
public class CelsiusConvert extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Converter";
        double inputTemp;
        try {
            inputTemp = Double.parseDouble(request.getParameter("temp"));
        } catch (NumberFormatException | NullPointerException e) {
            inputTemp = 0;
        }
        double tempF = convertToFahrenheit(inputTemp);

        out.println("<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1 class=\"mainTitle\"><a href=\"index.html\">" + title + "</a></H1>\n" +
                    "  <P>" + inputTemp +
                    "°C is equal to " +
                    String.format(Locale.US,"%.2f", tempF) +
                    "°F. </P>" +
                    "</BODY></HTML>");
    }

    /**
     * Converts given temperature in Celsius to Fahrenheit.
     * @param tempC Temperature in Fahrenheit
     * @return Temperature in Celsius
     */
    public double convertToFahrenheit(double tempC) {
        return tempC * 1.8 + 32;
    }
}
