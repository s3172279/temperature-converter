package TemperatureConvert;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

/**
 * Servlet that converts Fahrenheit to Celsius.
 */
public class FahrenheitConvert extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Converter";
        double inputTemp;
        try {
            inputTemp = Double.parseDouble(request.getParameter("temp"));
        } catch (NumberFormatException | NullPointerException e) {
            inputTemp = 0;
        }
        double tempC = convertToCelsius(inputTemp);

        out.println("<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1 class=\"mainTitle\"><a href=\"index.html\">" + title + "</a></H1>\n" +
                    "  <P>" + inputTemp +
                    "°F is equal to " +
                    String.format(Locale.US,  "%.2f", tempC) +
                    "°C. </P>" +
                    "</BODY></HTML>");
    }

    /**
     * Converts given temperature in Fahrenheit to Celsius.
     * @param tempF Temperature in Fahrenheit
     * @return Temperature in Celsius
     */
    double convertToCelsius(double tempF) {
        return (tempF- 32) * (5 / 9d);
    }
}
