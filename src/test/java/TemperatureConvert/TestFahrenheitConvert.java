package TemperatureConvert;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestFahrenheitConvert {
    @Test
    void testFahrenheitConverter() {
        FahrenheitConvert converter = new FahrenheitConvert();
        double tempC = converter.convertToCelsius(80);
        Assertions.assertEquals(26.66, tempC, 0.1,
                                "Conversion of 80 degrees Fahrenheit to Celsius");
    }
}
