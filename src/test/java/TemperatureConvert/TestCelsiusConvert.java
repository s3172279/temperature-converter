package TemperatureConvert;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestCelsiusConvert {
    @Test
    void testCelciusConverter() {
        CelsiusConvert converter = new CelsiusConvert();
        double tempF = converter.convertToFahrenheit(20);
        Assertions.assertEquals(68.0, tempF, 0.1, "Conversion of 20 degrees Celsius to Fahrenheit");
    }
}
